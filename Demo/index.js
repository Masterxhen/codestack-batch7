// index.js

// Data Types
// const = constant (meaning the value will not change)
// var and let = always changing values
var undefine
var object = {name:"codestack"}
var text = "Sabi ko na Barbie!" /* String */
var number = 4 /* Integer */
var bigInt = 2n ** 53n /* Big Int */
var bool = true
//

// Jquery $("#string").innerHTML
document.getElementById('string').innerHTML = text + " is a " + typeof text
document.getElementById('number').innerHTML = number + " is a " + typeof number
document.getElementById('bigInt').innerHTML = bigInt + " is a " + typeof bigInt
document.getElementById('undefine').innerHTML = undefine + " is a " + typeof undefine
document.getElementById('null').innerHTML = `${object.name} is a ${typeof object.name} from the ${object} is a ${typeof object} `
document.getElementById('bool').innerHTML = bool + " is a " + typeof bool

// ctrl + [
// crtl + ]

function operate () {
  // Mathematical Operations
  var number1 = parseInt(document.getElementById('textbox1').value)
  var number2 = parseInt(document.getElementById('textbox2').value)
  console.log("first number: " , number1)
  console.log("second number: " , number2)
  console.log("data type textbox1", typeof number1)
  console.log("data type textbox2", typeof number2)
  // Calling function names
  add(number1, number2)
  subtract(number1, number2)
  multiply(number1, number2)
  division(number1, number2)
  remainder(number1, number2)
}

// Functions
function add(number1, number2) {
  var sum = number1 + number2
  document.getElementById('add').innerHTML = number1 + " plus " + number2 + " = " + sum
}

function subtract(number1, number2) {
  var difference = number1 - number2
  document.getElementById('subtract').innerHTML = number1 + " minus " + number2 + " = " + difference
}

function multiply(number1, number2) {
  var product = number1 * number2
  document.getElementById('multiply').innerHTML = number1 + " times " + number2 + " = " + product
}

function division(number1, number2) {
  var quotient = number1 / number2
  document.getElementById('division').innerHTML = number1 + " divided by " + number2 + " = " + quotient
}

function remainder(number1, number2) {
  var remainder = number1 % number2
  document.getElementById('remainder').innerHTML = number1 + " divided by " + number2 + " = " + remainder
}
