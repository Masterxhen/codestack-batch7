# Welcome Learners from FullStackHybrid Development (Crash Course) Training Batch 7 💻🔥

## Before lesson starts, please download all the tools needed for this 2 Day Training
- [ ] [Download or Update Google Chrome Browser](https://www.google.com/chrome/) or [Download or Update Mozilla Firefox Browser](https://www.mozilla.org/en-US/firefox/new/)
- [ ] [Visual Studio Code](https://code.visualstudio.com/)
- [ ] [Node JS LTS Version](https://nodejs.org/en/)
- [ ] [Yarn](https://yarnpkg.com/en/docs/install)
- [ ] [Vue JS Devtools (For Google Chrome Browsers Only)](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en) or [Vue JS Devtools (For Mozilla Firefox Browsers Only)](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/)
