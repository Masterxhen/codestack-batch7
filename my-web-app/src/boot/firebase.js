// import something here
import firebase from "firebase/app"
import "firebase/firestore"
import { firestorePlugin } from "vuefire"

var firebaseConfig = {
  apiKey: "AIzaSyAF_sQz-MTGIuwvoIMQnJipl8ZygunQbVE",
  authDomain: "batch7-codestack-1c883.firebaseapp.com",
  databaseURL: "https://batch7-codestack-1c883.firebaseio.com",
  projectId: "batch7-codestack-1c883",
  storageBucket: "batch7-codestack-1c883.appspot.com",
  messagingSenderId: "263907382931",
  appId: "1:263907382931:web:e601ead1a198ba240c9f66"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore()
// "async" is optional
export default async ({ Vue }) => {
  // something to do
  Vue.use(firestorePlugin)
  Vue.prototype.$db = db
}
