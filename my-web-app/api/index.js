const express = require("express")
const parser = require("body-parser")
const cors = require("cors")
const app = express()
const port = 5000;

app.use(parser.json())
app.use(cors())


var number = 0
app.get('/all', function(request, response) {
  response.send("hello Learners " + number )
  number++
})

app.listen(port, function () {
  console.log("server running on port: ", port)
})
